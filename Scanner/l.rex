/**
 * Samuel Philipp
 * Micha Piertzig
 * Jan-Eric Gaidusch
 * 
 * 
 */

/* Project:  COCKTAIL training
 * Descr:    a simple scanner generated with rex
 * Kind:     REX scanner specification (solution)
 * Author:   Dr. Juergen Vollmer <juergen.vollmer@informatik-vollmer.de>
 * $Id: l.rex.in,v 1.10 2014/05/14 08:46:39 vollmer Exp $
 */

SCANNER l_scan

EXPORT {
/* code to be put into Scanner.h */

# include "Position.h"

/* Token Attributes.
 * For each token with user defined attributes, we need a typedef for the
 * token attributes.
 * The first struct-field must be of type tPosition!
 */
typedef struct {tPosition Pos; char* Value;} tint_const;
typedef struct {tPosition Pos; char* Value;} tstring_const;
typedef struct {tPosition Pos; char* Value;} tapp_name_const;
typedef struct {tPosition Pos; char* Value;} tfloat_const;
typedef struct {tPosition Pos; char* Value;} tbez;
typedef struct {tPosition Pos; char* Value;} top;
typedef struct {tPosition Pos; char* Value;} tkla_auf;
typedef struct {tPosition Pos; char* Value;} tkla_zu;
typedef struct {tPosition Pos; char* Value;} tzuw;
typedef struct {tPosition Pos; char* Value;} tint_key;

/* There is only one "actual" token, during scanning. Therfore
 * we use a UNION of all token-attributes as data type for that unique
 * token-attribute variable.
 * All token (with and without user defined attributes) have one
 * attribute: the source position:
 *     tPosition     Position;
 */
typedef union {
  tPosition     Position;
  tint_const    int_const;
  tstring_const string_const;
  tfloat_const float_const;
  tbez bez;
  top op;
  tkla_auf kla_auf;
  tkla_zu kla_zu;
  tzuw zuw;
  tapp_name_const app_name_const;
  tint_key int_key;
} l_scan_tScanAttribute;

/* Tokens are coded as int's, with values >=0
 * The value 0 is reserved for the EofToken, which is defined automatically
 */
# define tok_int_const    1
# define tok_string_const 2
# define tok_app_name_const 3
# define tok_float_const 4
# define tok_bez 5
# define tok_op 6
# define tok_kla_auf 7
# define tok_kla_zu 8
# define tok_zuw 9
# define tok_int_key 10
 
} // EXPORT

GLOBAL {
  # include <stdlib.h>
  # include "rString.h"
} // GLOBAL

LOCAL {
 /* user-defined local variables of the generated GetToken routine */
  # define MAX_STRING_LEN 2048
  char string [MAX_STRING_LEN+1];
  int appNameLength = 0;
  int stringLength = 0;
  int commentLevel = 0;
}  // LOCAL

DEFAULT {
  /* What happens if no scanner rule matches the input */
  WritePosition (stderr, l_scan_Attribute.Position);
  fprintf (stderr, " Illegal character [%c]\n", *l_scan_TokenPtr);
} // DEFAULT

EOF {
  /* What should be done if the end-of-input-file has been reached? */

  /* E.g.: check hat strings and comments are closed. */
  switch (yyStartState) {
  case STD:
    /* ok */
    break;
  case APP_NAME:
    WritePosition (stderr, l_scan_Attribute.Position);
    fprintf (stderr, " Nicht abgeschlossener App-Name\n");
   break;
  case STRING:
    WritePosition (stderr, l_scan_Attribute.Position);
    fprintf (stderr, " Nicht abgeschlossener String\n");
   break;
  case COMMENT:
    WritePosition (stderr, l_scan_Attribute.Position);
    fprintf (stderr, " Nicht abgeschlossener Kommentar\n");
    break;
  default:
    Message ("OOPS: that should not happen!!",
         xxFatal, l_scan_Attribute.Position);
    break;
  }

  /* implicit: return the EofToken */
} // EOF

DEFINE  /* some abbreviations */
  digit  = {0-9}.
  letter = {a-zA-Z}.
  string = - {"\\\n\r\f} .

/* define start states, note STD is defined by default, separate several states by a comma */
START APP_NAME, STRING, COMMENT

RULE

/* App Name */
#STD# "#" :
{
    yyStart(APP_NAME);
    appNameLength = 0;
}

#APP_NAME# (letter|digit|" ")+ :
{
      appNameLength += l_scan_GetWord (&string[appNameLength]);
}

#APP_NAME# ; :
{
    yyStart(STD);
    string[appNameLength] = '\0';
    l_scan_Attribute.app_name_const.Value = malloc (appNameLength);
    strcpy (l_scan_Attribute.app_name_const.Value, string);
    return tok_app_name_const;
}

/* Comments */
#STD,COMMENT# "<!--" :
{ 
    yyStart (COMMENT);
    commentLevel ++;
}

#COMMENT# ANY :
{
}

#COMMENT# "-->" :
{ 
    commentLevel --;
    if (commentLevel == 0) {
	yyStart (STD);
    }
}

/* int key word*/
#STD# "int" :
{
    l_scan_Attribute.int_key.Value = malloc (l_scan_TokenLength+1);
    l_scan_GetWord (l_scan_Attribute.int_key.Value);
    return tok_int_key;
}

/* Integers */
#STD# digit+ :
{
    l_scan_Attribute.int_const.Value = malloc (l_scan_TokenLength+1);
    l_scan_GetWord (l_scan_Attribute.int_const.Value);
    return tok_int_const;
}

/* Float*/
#STD# digit + "." digit * (("E"|"e") ("+"|"-") ? digit +) ? :
{
    l_scan_Attribute.float_const.Value = malloc (l_scan_TokenLength+1);
    l_scan_GetWord (l_scan_Attribute.float_const.Value);
    return tok_float_const;
}

/* Bezeichner */
#STD# letter (letter | digit) * :
{
    l_scan_Attribute.bez.Value = malloc (l_scan_TokenLength+1);
    l_scan_GetWord (l_scan_Attribute.bez.Value);
    return tok_bez;
}

/* zuweisung*/
#STD# "=" :
{
    l_scan_Attribute.zuw.Value = malloc (l_scan_TokenLength+1);
    l_scan_GetWord (l_scan_Attribute.zuw.Value);
    return tok_zuw;
}

/* Operator*/
#STD# {+ | - | * | / | < | >} :
{
    l_scan_Attribute.op.Value = malloc (l_scan_TokenLength+1);
    l_scan_GetWord (l_scan_Attribute.op.Value);
    return tok_op;
}

/* Klammer Auf*/
#STD# {(} :
{
    l_scan_Attribute.kla_auf.Value = malloc (l_scan_TokenLength+1);
    l_scan_GetWord (l_scan_Attribute.kla_auf.Value);
    return tok_kla_auf;
}

/* Klammer Zu*/
#STD# {)} :
{
    l_scan_Attribute.kla_zu.Value = malloc (l_scan_TokenLength+1);
    l_scan_GetWord (l_scan_Attribute.kla_zu.Value);
    return tok_kla_zu;
}

#STD# \" :
{
    yyStart (STRING);
    stringLength = 0;
}
    
#STRING# string* :
{
    stringLength += l_scan_GetWord (&string[stringLength]);
}
    
#STRING# \" :
{
    yyStart(STD);
    string[stringLength] = '\0';
    l_scan_Attribute.string_const.Value = malloc (stringLength);
    strcpy (l_scan_Attribute.string_const.Value, string);
    return tok_string_const;
}

#STRING# \\ \" :
{
    string[stringLength++] = '"';
}

/* Please add rules for: (don't forget to adapt main()) */
/* Float numbers */

/* case insensitive keywords: BEGIN PROCEDURE END CASE */

/* identifiers */

/* comment up to end of line */

/* C-style comment */

/* Modula2-style nested comment */

/* double quote delimited strings */
/**********************************************************************/