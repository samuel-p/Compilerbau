/* $Id: Scanner.c,v 2.67 2009/10/13 14:13:39 grosch Exp $ */

# define rbool		char
# define rtrue		1
# define rfalse		0
# define StdIn		0

# include "l_scan.h"

# ifndef EXTERN_C_BEGIN
# define EXTERN_C_BEGIN
# define EXTERN_C_END
# endif

EXTERN_C_BEGIN
#    include "l_scanSource.h"
#    include "rSystem.h"
#    include "General.h"
#    include "DynArray.h"
#    include "Position.h"
#    include "Errors.h"
EXTERN_C_END

# include <stdio.h>
# include <stdlib.h>
# include <stddef.h>

# define yyStart(State)	{ yyPreviousStart = yyStartState; yyStartState = State;}
# define yyPrevious	{ yyStateRange s = yyStartState; \
			yyStartState = yyPreviousStart; yyPreviousStart = s; }
# define yyEcho		{ (void) fwrite (l_scan_TokenPtr, sizeof (yytChar), \
			l_scan_TokenLength, stdout); }
# define yyEol(Column)	{ yyLineCount ++; \
			yyLineStart = (yytusChar *) l_scan_TokenPtr + \
			l_scan_TokenLength - 1 - (Column); }
# if l_scan_xxMaxCharacter < 256
#  define output(c)	(void) putchar ((int) c)
# else
#  define output(c)	(void) printf ("%lc", c)
# endif
# define yyColumn(Ptr)	((int) ((Ptr) - (yytChar *) yyLineStart))
# define yyOffset(Ptr)	(yyFileOffset + ((Ptr) - yyChBufferStart2))

# define yytChar	l_scan_xxtChar
# define yytusChar	l_scan_xxtusChar

# define yyDNoState		0
# define yyFirstCh	(yytusChar) '\0'
# define yyEolCh	(yytusChar) '\12'
# define yyEobCh	(yytusChar) '\177'
# define yyDStateCount	49
# define yyTableSize	924
# define yyEobState	20
# define yyDefaultState	21
# define yyToClassArraySize	0
# define STD	1
# define APP_NAME	3
# define STRING	5
# define COMMENT	7
# define xxGetWord
# define xxinput

static void yyExit ARGS ((void))
{ rExit (1); }

typedef unsigned short	yyStateRange;
typedef struct { yyStateRange yyCheck, yyNext; } yyCombType;

	yytChar *	l_scan_TokenPtr	;
	int		l_scan_TokenLength	;
	l_scan_tScanAttribute	l_scan_Attribute	;
	void		(* l_scan_Exit)	ARGS ((void)) = yyExit;

static	void		yyInitialize	ARGS ((void));
static	void		yyErrorMessage	ARGS ((int yyErrorCode));
static	yytChar		input		ARGS ((void));
static	void		unput		ARGS ((yytChar));
static	void		yyLess		ARGS ((int));

static	yyCombType	yyComb		[yyTableSize   + 1] = {
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   1,   23}, 
{   1,   22}, {   0,    0}, {   0,    0}, {   3,   23}, {   3,   22}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   5,   24}, {   5,   22}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   1,   26}, {   0,    0}, {   1,   33}, 
{   1,   49}, {   3,   27}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   1,   35}, {   1,   34}, {   1,   12}, {   1,   12}, {   5,   28}, 
{   0,    0}, {   5,   32}, {   1,   12}, {   1,   13}, {   1,   13}, 
{   1,   13}, {   1,   13}, {   1,   13}, {   1,   13}, {   1,   13}, 
{   1,   13}, {   1,   13}, {   1,   13}, {   0,    0}, {   0,    0}, 
{   1,   43}, {   1,   36}, {   1,   12}, {   3,   48}, {  13,   17}, 
{   0,    0}, {  13,   13}, {  13,   13}, {  13,   13}, {  13,   13}, 
{  13,   13}, {  13,   13}, {  13,   13}, {  13,   13}, {  13,   13}, 
{  13,   13}, {  15,   15}, {  15,   15}, {  15,   15}, {  15,   15}, 
{  15,   15}, {  15,   15}, {  15,   15}, {  15,   15}, {  15,   15}, 
{  15,   15}, {  18,   16}, {   0,    0}, {  18,   16}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   5,   30}, 
{   1,   37}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,   25}, {   1,   12}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,   29}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,   40}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,   47}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {  19,   20}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, {   7,    9}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  30,   31}, {  10,   10}, {  37,   38}, {  38,   39}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  40,   41}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  41,   42}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  43,   44}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, {  10,   10}, 
{  10,   10}, {  11,   11}, {  44,   45}, {  45,   46}, {  47,   44}, 
{   0,    0}, {   0,    0}, {  17,   17}, {  17,   17}, {  17,   17}, 
{  17,   17}, {  17,   17}, {  17,   17}, {  17,   17}, {  17,   17}, 
{  17,   17}, {  17,   17}, {  11,   11}, {  11,   11}, {  11,   11}, 
{  11,   11}, {  11,   11}, {  11,   11}, {  11,   11}, {  11,   11}, 
{  11,   11}, {  11,   11}, {   0,    0}, {  17,   18}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {  11,   11}, 
{  11,   11}, {  11,   11}, {  11,   11}, {  11,   11}, {  11,   11}, 
{  11,   11}, {  11,   11}, {  11,   11}, {  11,   11}, {  11,   11}, 
{  11,   11}, {  11,   11}, {  11,   11}, {  11,   11}, {  11,   11}, 
{  11,   11}, {  11,   11}, {  11,   11}, {  11,   11}, {  11,   11}, 
{  11,   11}, {  11,   11}, {  11,   11}, {  11,   11}, {  11,   11}, 
{  17,   18}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {  11,   11}, {  11,   11}, {  11,   11}, {  11,   11}, 
{  11,   11}, {  11,   11}, {  11,   11}, {  11,   11}, {  11,   11}, 
{  11,   11}, {  11,   11}, {  11,   11}, {  11,   11}, {  11,   11}, 
{  11,   11}, {  11,   11}, {  11,   11}, {  11,   11}, {  11,   11}, 
{  11,   11}, {  11,   11}, {  11,   11}, {  11,   11}, {  11,   11}, 
{  11,   11}, {  11,   11}, {  14,   14}, {  14,   14}, {  14,   14}, 
{  14,   14}, {  14,   14}, {  14,   14}, {  14,   14}, {  14,   14}, 
{  14,   14}, {  14,   14}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {  14,   14}, 
{  14,   14}, {  14,   14}, {  14,   14}, {  14,   14}, {  14,   14}, 
{  14,   14}, {  14,   14}, {  14,   14}, {  14,   14}, {  14,   14}, 
{  14,   14}, {  14,   14}, {  14,   14}, {  14,   14}, {  14,   14}, 
{  14,   14}, {  14,   14}, {  14,   14}, {  14,   14}, {  14,   14}, 
{  14,   14}, {  14,   14}, {  14,   14}, {  14,   14}, {  14,   14}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {  14,   14}, {  14,   14}, {  14,   14}, {  14,   14}, 
{  14,   14}, {  14,   14}, {  14,   14}, {  14,   14}, {  14,   14}, 
{  14,   14}, {  14,   14}, {  14,   14}, {  14,   14}, {  14,   14}, 
{  14,   14}, {  14,   14}, {  14,   14}, {  14,   14}, {  14,   14}, 
{  14,   14}, {  14,   14}, {  14,   14}, {  14,   14}, {  14,   14}, 
{  14,   14}, {  14,   14}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
{   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, {   0,    0}, 
};
static	yyCombType *	yyBasePtr	[yyDStateCount + 1] = { 0,
& yyComb [   0], & yyComb [   0], & yyComb [   4], & yyComb [   0], 
& yyComb [  12], & yyComb [   0], & yyComb [ 114], & yyComb [   0], 
& yyComb [   0], & yyComb [ 370], & yyComb [ 594], & yyComb [   0], 
& yyComb [  18], & yyComb [ 669], & yyComb [  28], & yyComb [   0], 
& yyComb [ 584], & yyComb [  43], & yyComb [ 114], & yyComb [   0], 
& yyComb [   0], & yyComb [   0], & yyComb [   0], & yyComb [   0], 
& yyComb [   0], & yyComb [   0], & yyComb [   0], & yyComb [   0], 
& yyComb [   0], & yyComb [ 346], & yyComb [   0], & yyComb [   0], 
& yyComb [   0], & yyComb [   0], & yyComb [   0], & yyComb [   0], 
& yyComb [ 272], & yyComb [ 267], & yyComb [   0], & yyComb [ 359], 
& yyComb [ 400], & yyComb [   0], & yyComb [ 464], & yyComb [ 582], 
& yyComb [ 583], & yyComb [   0], & yyComb [ 596], & yyComb [   0], 
& yyComb [   0], 
};
static	yyStateRange	yyDefault	[yyDStateCount + 1] = { 0,
   14,     1,    11,     3,    10,     5,     1,     7,    19,    19, 
   19,    19,    19,    19,    19,    15,    19,    15,     0,     0, 
    0,     0,     0,    10,     9,     0,    11,    10,     9,    19, 
    0,     0,     0,     0,     0,     0,    14,    14,    14,     9, 
   19,     0,    12,    19,    19,     0,     9,     0,     0, 
};
static	yyStateRange	yyEobTrans	[yyDStateCount + 1] = { 0,
    0,     0,     0,     0,    10,    10,     9,     9,     0,    10, 
    0,     0,     0,     0,     0,     0,     0,     0,     0,     0, 
    0,     0,     0,    10,     0,     0,     0,    10,     0,     0, 
    0,     0,     0,     0,     0,     0,     0,     0,     0,     0, 
    0,     0,     0,     0,     0,     0,     0,     0,     0, 
};

# if l_scan_xxMaxCharacter < 256
#  define yyGetLine	l_scan_GetLine
# else
#  define yyGetLine	l_scan_GetWLine
# endif

# if yyToClassArraySize == 0
#  define yyToClass(x) (x)
# else
typedef	unsigned short	yytCharClass;

static	yytCharClass	yyToClassArray	[yyToClassArraySize] = {
};
#  if l_scan_xxMaxCharacter < yyToClassArraySize
#   define yyToClass(x) (yyToClassArray [x])
#  else
#   define yyToClass(x) \
	((x) < yyToClassArraySize ? yyToClassArray [x] : yyToClassFunction (x))

static	yytusChar	yyToClassUpb	[yyToClassRangeSize] = {
};
static	yytCharClass	yyToClassClass	[yyToClassRangeSize] = {
};

static yytCharClass yyToClassFunction
# ifdef HAVE_ARGS
   (yytusChar yyCh)
# else
   (yyCh) yytusChar yyCh;
# endif
{
   register int yyLower = 1, yyUpper = yyToClassRangeSize;
   while (yyUpper - yyLower > 1) {			/* binary search */
      register int yyMiddle = (yyLower + yyUpper) / 2;
      if (yyToClassUpb [yyMiddle] < yyCh)
	 yyLower = yyMiddle;
      else
	 yyUpper = yyMiddle;
   }
   if (yyToClassUpb [yyLower - 1] < yyCh && yyCh <= yyToClassUpb [yyLower])
      return yyToClassClass [yyLower];
   else
      return yyToClassClass [yyUpper];
}

#  endif
# endif

static	yyStateRange	yyStartState	= STD;
static	yyStateRange	yyPreviousStart	= STD;
static	int		yySourceFile	= StdIn;
static	rbool		yyEof		= rfalse;
static	long		yyBytesRead	= 0;
static	long		yyFileOffset	= 0;
static	unsigned int	yyLineCount	= 1;
static	yytusChar *	yyLineStart	;
static	yytChar *	yyChBufferStart2;

					/* Start State Stack: StStSt	*/

# if defined xxyyPush | defined xxyyPop
# define		yyInitStStStackSize	16

static	yyStateRange *	yyStStStackPtr	;
static	unsigned long	yyStStStackSize	= 0;
static	unsigned int	yyStStStackIdx	= 0;
# endif

# ifdef xxyyPush
static void yyPush
# ifdef HAVE_ARGS
   (yyStateRange yyState)
# else
   (yyState) yyStateRange yyState;
# endif
{
   if (yyStStStackIdx == yyStStStackSize) {
      if (yyStStStackSize == 0) {
	 yyStStStackSize = yyInitStStStackSize;
	 MakeArray ((char * *) & yyStStStackPtr, & yyStStStackSize,
			(unsigned long) sizeof (yyStateRange));
      } else {
	 ExtendArray ((char * *) & yyStStStackPtr, & yyStStStackSize,
			(unsigned long) sizeof (yyStateRange));
      }
      if (yyStStStackPtr == NULL) yyErrorMessage (xxScannerOutOfMemory);
   }
   yyStStStackPtr [yyStStStackIdx ++] = yyStartState;
   yyStart (yyState);
}
# endif

# ifdef xxyyPop
static void yyPop ARGS ((void))
{
   yyPreviousStart = yyStartState;
   if (yyStStStackIdx > 0)
      yyStartState = yyStStStackPtr [-- yyStStStackIdx];
   else
      yyErrorMessage (xxStartStackUnderflow);
}
# endif

static void yyTab1	ARGS ((int yya));

# define yyTab		yyTab1 (0)
# define yyTab2(a,b)	yyTab1 (a)

/* line 76 "l.rex" */

  # include <stdlib.h>
  # include "rString.h"

/* line 411 "l_scan.c" */

# ifndef yySetPosition
# define yySetPosition l_scan_Attribute.Position.Line = yyLineCount; \
l_scan_Attribute.Position.Column = (int) ((yytusChar *) l_scan_TokenPtr - yyLineStart);
# endif

# undef yyTab
# undef yyTab2

# ifndef yyInitBufferSize
# define yyInitBufferSize	1024 * 8 + 256
# endif
# ifndef yyInitFileStackSize
# define yyInitFileStackSize	8
# endif
# ifndef yyTabSpace
# define yyTabSpace		8
# endif

static void yyTab1
# ifdef HAVE_ARGS
   (int yya)
# else
   (yya) int yya;
# endif
   { yyLineStart -= (yyTabSpace - 1 - ((yytusChar *) l_scan_TokenPtr -
	yyLineStart + yya - 1)) & (yyTabSpace - 1); }

# define yyTab		yyLineStart -= (yyTabSpace - 1 - \
((yytusChar *) l_scan_TokenPtr - yyLineStart - 1)) & (yyTabSpace - 1)
# define yyTab1(a)	yyLineStart -= (yyTabSpace - 1 - \
((yytusChar *) l_scan_TokenPtr - yyLineStart + (a) - 1)) & (yyTabSpace - 1)
# define yyTab2(a,b)	yyLineStart -= (yyTabSpace - 1 - \
((yytusChar *) l_scan_TokenPtr - yyLineStart + (a) - 1)) & (yyTabSpace - 1)

# ifndef EBCDIC
#  if l_scan_xxMaxCharacter < 256
#   include <ctype.h>
#   define yyToUpper(x)	toupper (x)
#   define yyToLower(x)	tolower (x)
#  else
#   include <wctype.h>
#   define yyToUpper(x)	towupper (x)
#   define yyToLower(x)	towlower (x)
#  endif
# else
#  define yyToLower(x)	yyToLowerArray [x]
#  define yyToUpper(x)	yyToUpperArray [x]

# ifdef xxGetLower
static	yytusChar	yyToLowerArray	[] = {
'\x00', '\x01', '\x02', '\x03', '\x04', '\x05', '\x06', '\x07',
'\x08', '\x09', '\x0A', '\x0B', '\x0C', '\x0D', '\x0E', '\x0F',
'\x10', '\x11', '\x12', '\x13', '\x14', '\x15', '\x16', '\x17',
'\x18', '\x19', '\x1A', '\x1B', '\x1C', '\x1D', '\x1E', '\x1F',
'\x20', '\x21', '\x22', '\x23', '\x24', '\x25', '\x26', '\x27',
'\x28', '\x29', '\x2A', '\x2B', '\x2C', '\x2D', '\x2E', '\x2F',
'\x30', '\x31', '\x32', '\x33', '\x34', '\x35', '\x36', '\x37',
'\x38', '\x39', '\x3A', '\x3B', '\x3C', '\x3D', '\x3E', '\x3F',
'\x40', '\x41', '\x42', '\x43', '\x44', '\x45', '\x46', '\x47',
'\x48', '\x49', '\x4A', '\x4B', '\x4C', '\x4D', '\x4E', '\x4F',
'\x50', '\x51', '\x52', '\x53', '\x54', '\x55', '\x56', '\x57',
'\x58', '\x59', '\x5A', '\x5B', '\x5C', '\x5D', '\x5E', '\x5F',
'\x60', '\x61', '\x62', '\x63', '\x64', '\x65', '\x66', '\x67',
'\x68', '\x69', '\x6A', '\x6B', '\x6C', '\x6D', '\x6E', '\x6F',
'\x70', '\x71', '\x72', '\x73', '\x74', '\x75', '\x76', '\x77',
'\x78', '\x79', '\x7A', '\x7B', '\x7C', '\x7D', '\x7E', '\x7F',
'\x80',    'a',    'b',    'c',    'd',    'e',    'f',    'g',
   'h',    'i', '\x8A', '\x8B', '\x8C', '\x8D', '\x8E', '\x8F',
'\x90',    'j',    'k',    'l',    'm',    'n',    'o',    'p',
   'q',    'r', '\x9A', '\x9B', '\x9C', '\x9D', '\x9E', '\x9F',
'\xA0', '\xA1',    's',    't',   'u',     'v',    'w',    'x',
   'y',    'z', '\xAA', '\xAB', '\xAC', '\xAD', '\xAE', '\xAF',
'\xB0', '\xB1', '\xB2', '\xB3', '\xB4', '\xB5', '\xB6', '\xB7',
'\xB8', '\xB9', '\xBA', '\xBB', '\xBC', '\xBD', '\xBE', '\xBF',
'\xC0',    'a',    'b',    'c',    'd',    'e',    'f',    'g',
   'h',    'i', '\xCA', '\xCB', '\xCC', '\xCD', '\xCE', '\xCF',
'\xD0',    'j',    'k',    'l',    'm',    'n',    'o',    'p',
   'q',    'r', '\xDA', '\xDB', '\xDC', '\xDD', '\xDE', '\xDF',
'\xE0', '\xE1',    's',    't',    'u',    'v',    'w',    'x',
   'y',    'z', '\xEA', '\xEB', '\xEC', '\xED', '\xEE', '\xEF',
'\xF0', '\xF1', '\xF2', '\xF3', '\xF4', '\xF5', '\xF6', '\xF7',
'\xF8', '\xF9', '\xFA', '\xFB', '\xFC', '\xFD', '\xFE', '\xFF'
};
# endif

# ifdef xxGetUpper
static	yytusChar	yyToUpperArray	[] = {
'\x00', '\x01', '\x02', '\x03', '\x04', '\x05', '\x06', '\x07',
'\x08', '\x09', '\x0A', '\x0B', '\x0C', '\x0D', '\x0E', '\x0F',
'\x10', '\x11', '\x12', '\x13', '\x14', '\x15', '\x16', '\x17',
'\x18', '\x19', '\x1A', '\x1B', '\x1C', '\x1D', '\x1E', '\x1F',
'\x20', '\x21', '\x22', '\x23', '\x24', '\x25', '\x26', '\x27',
'\x28', '\x29', '\x2A', '\x2B', '\x2C', '\x2D', '\x2E', '\x2F',
'\x30', '\x31', '\x32', '\x33', '\x34', '\x35', '\x36', '\x37',
'\x38', '\x39', '\x3A', '\x3B', '\x3C', '\x3D', '\x3E', '\x3F',
'\x40', '\x41', '\x42', '\x43', '\x44', '\x45', '\x46', '\x47',
'\x48', '\x49', '\x4A', '\x4B', '\x4C', '\x4D', '\x4E', '\x4F',
'\x50', '\x51', '\x52', '\x53', '\x54', '\x55', '\x56', '\x57',
'\x58', '\x59', '\x5A', '\x5B', '\x5C', '\x5D', '\x5E', '\x5F',
'\x60', '\x61', '\x62', '\x63', '\x64', '\x65', '\x66', '\x67',
'\x68', '\x69', '\x6A', '\x6B', '\x6C', '\x6D', '\x6E', '\x6F',
'\x70', '\x71', '\x72', '\x73', '\x74', '\x75', '\x76', '\x77',
'\x78', '\x79', '\x7A', '\x7B', '\x7C', '\x7D', '\x7E', '\x7F',
'\x80',    'A',    'B',    'C',    'D',    'E',    'F',    'G',
   'H',    'I', '\x8A', '\x8B', '\x8C', '\x8D', '\x8E', '\x8F',
'\x90',    'J',    'K',    'L',    'M',    'N',    'O',    'P',
   'Q',    'R', '\x9A', '\x9B', '\x9C', '\x9D', '\x9E', '\x9F',
'\xA0', '\xA1',    'S',    'T',    'U',    'V',    'W',    'X',
   'Y',    'Z', '\xAA', '\xAB', '\xAC', '\xAD', '\xAE', '\xAF',
'\xB0', '\xB1', '\xB2', '\xB3', '\xB4', '\xB5', '\xB6', '\xB7',
'\xB8', '\xB9', '\xBA', '\xBB', '\xBC', '\xBD', '\xBE', '\xBF',
'\xC0',    'A',    'B',    'C',    'D',    'E',    'F',    'G',
   'H',    'I', '\xCA', '\xCB', '\xCC', '\xCD', '\xCE', '\xCF',
'\xD0',    'J',    'K',    'L',    'M',    'N',    'O',    'P',
   'Q',    'R', '\xDA', '\xDB', '\xDC', '\xDD', '\xDE', '\xDF',
'\xE0', '\xE1',    'S',    'T',    'U',    'V',    'W',    'X',
   'Y',    'Z', '\xEA', '\xEB', '\xEC', '\xED', '\xEE', '\xEF',
'\xF0', '\xF1', '\xF2', '\xF3', '\xF4', '\xF5', '\xF6', '\xF7',
'\xF8', '\xF9', '\xFA', '\xFB', '\xFC', '\xFD', '\xFE', '\xFF'
};
# endif

# endif

static	yyStateRange	yyInitStateStack [4] = { yyDefaultState };
static	yyStateRange *	yyStateStack	= yyInitStateStack;
static	unsigned long	yyStateStackSize= 0;

static	yytusChar	yyInitChBuffer [] = {
   '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
   '\0', '\0', '\0', '\0', '\0', '\0', '\0', yyEolCh, yyEobCh, '\0', };
static	yytusChar *	yyChBufferPtr	= yyInitChBuffer;
static	unsigned long	yyChBufferSize	= 0;
static	yytusChar *	yyChBufferStart	= & yyInitChBuffer [16];
static	yytusChar *	yyChBufferIndex	= & yyInitChBuffer [16];

# if yyInitFileStackSize != 0
typedef	struct {
	int		yySourceFile	;
	rbool		yyEof		;
	yytusChar *	yyChBufferPtr	;
	yytusChar *	yyChBufferStart	;
	unsigned long	yyChBufferSize	;
	yytusChar *	yyChBufferIndex	;
	long		yyBytesRead	;
	long		yyFileOffset	;
	unsigned int	yyLineCount	;
	yytusChar *	yyLineStart	;
	}		yytFileStack	;

static	yytFileStack *	yyFileStack	;
static	unsigned long	yyFileStackSize	= 0;
static	yytFileStack *	yyFileStackPtr	;
# endif

int l_scan_GetToken ARGS ((void))
{
   register	yyStateRange	yyState;
   register	yyStateRange *	yyStatePtr;
   register	yytusChar *	yyChBufferIndexReg;
   register	yyCombType * *	yyBasePtrReg = yyBasePtr;
/* line 81 "l.rex" */

 /* user-defined local variables of the generated GetToken routine */
  # define MAX_STRING_LEN 2048
  char string [MAX_STRING_LEN+1];
  int appNameLength = 0;
  int stringLength = 0;
  int commentLevel = 0;

/* line 583 "l_scan.c" */

yyBegin:
   yyState		= yyStartState;		/* initialize */
   yyStatePtr		= & yyStateStack [1];
   yyChBufferIndexReg	= yyChBufferIndex;
   l_scan_TokenPtr		= (yytChar *) yyChBufferIndexReg;

   /* ASSERT yyChBuffer [yyChBufferIndex] == first character */

yyContinue:		/* continue after sentinel or skipping blanks */
   for (;;) {		/* execute as many state transitions as possible */
			/* determine next state and get next character */
      register yyCombType * yyTablePtr =
		      yyBasePtrReg [yyState] + yyToClass (* yyChBufferIndexReg);
      if (yyTablePtr->yyCheck == yyState) {
	 yyState = yyTablePtr->yyNext;		/* determine next state */
	 * yyStatePtr ++ = yyState;		/* push state */
	 yyChBufferIndexReg ++;			/* get next character */
	 goto yyContinue;
      }
      if ((yyState = yyDefault [yyState]) == yyDNoState) break;
   }

   for (;;) {				/* search for last final state */
      l_scan_TokenLength =
	    (int) (yyChBufferIndexReg - (yytusChar *) l_scan_TokenPtr);
      yyChBufferIndex = yyChBufferIndexReg;
switch (* -- yyStatePtr) {
case 49:;
yySetPosition
/* line 137 "l.rex" */
{
    yyStart(APP_NAME);
    appNameLength = 0;

/* line 619 "l_scan.c" */
} goto yyBegin;
case 11:;
case 27:;
yySetPosition
/* line 143 "l.rex" */
{
      appNameLength += l_scan_GetWord (&string[appNameLength]);

/* line 628 "l_scan.c" */
} goto yyBegin;
case 48:;
yySetPosition
/* line 148 "l.rex" */
{
    yyStart(STD);
    string[appNameLength] = '\0';
    l_scan_Attribute.app_name_const.Value = malloc (appNameLength);
    strcpy (l_scan_Attribute.app_name_const.Value, string);
    return tok_app_name_const;

/* line 640 "l_scan.c" */
} goto yyBegin;
case 46:;
yySetPosition
/* line 158 "l.rex" */
{ 
    yyStart (COMMENT);
    commentLevel ++;

/* line 649 "l_scan.c" */
} goto yyBegin;
case 9:;
case 25:;
case 29:;
case 40:;
case 47:;
yySetPosition
/* line 164 "l.rex" */
{

/* line 660 "l_scan.c" */
} goto yyBegin;
case 42:;
yySetPosition
/* line 168 "l.rex" */
{ 
    commentLevel --;
    if (commentLevel == 0) {
	yyStart (STD);
    }

/* line 671 "l_scan.c" */
} goto yyBegin;
case 39:;
yySetPosition
/* line 177 "l.rex" */
{
    l_scan_Attribute.int_key.Value = malloc (l_scan_TokenLength+1);
    l_scan_GetWord (l_scan_Attribute.int_key.Value);
    return tok_int_key;

/* line 681 "l_scan.c" */
} goto yyBegin;
case 13:;
yySetPosition
/* line 185 "l.rex" */
{
    l_scan_Attribute.int_const.Value = malloc (l_scan_TokenLength+1);
    l_scan_GetWord (l_scan_Attribute.int_const.Value);
    return tok_int_const;

/* line 691 "l_scan.c" */
} goto yyBegin;
case 15:;
case 17:;
yySetPosition
/* line 193 "l.rex" */
{
    l_scan_Attribute.float_const.Value = malloc (l_scan_TokenLength+1);
    l_scan_GetWord (l_scan_Attribute.float_const.Value);
    return tok_float_const;

/* line 702 "l_scan.c" */
} goto yyBegin;
case 14:;
case 37:;
case 38:;
yySetPosition
/* line 201 "l.rex" */
{
    l_scan_Attribute.bez.Value = malloc (l_scan_TokenLength+1);
    l_scan_GetWord (l_scan_Attribute.bez.Value);
    return tok_bez;

/* line 714 "l_scan.c" */
} goto yyBegin;
case 36:;
yySetPosition
/* line 209 "l.rex" */
{
    l_scan_Attribute.zuw.Value = malloc (l_scan_TokenLength+1);
    l_scan_GetWord (l_scan_Attribute.zuw.Value);
    return tok_zuw;

/* line 724 "l_scan.c" */
} goto yyBegin;
case 12:;
case 43:;
yySetPosition
/* line 217 "l.rex" */
{
    l_scan_Attribute.op.Value = malloc (l_scan_TokenLength+1);
    l_scan_GetWord (l_scan_Attribute.op.Value);
    return tok_op;

/* line 735 "l_scan.c" */
} goto yyBegin;
case 35:;
yySetPosition
/* line 225 "l.rex" */
{
    l_scan_Attribute.kla_auf.Value = malloc (l_scan_TokenLength+1);
    l_scan_GetWord (l_scan_Attribute.kla_auf.Value);
    return tok_kla_auf;

/* line 745 "l_scan.c" */
} goto yyBegin;
case 34:;
yySetPosition
/* line 233 "l.rex" */
{
    l_scan_Attribute.kla_zu.Value = malloc (l_scan_TokenLength+1);
    l_scan_GetWord (l_scan_Attribute.kla_zu.Value);
    return tok_kla_zu;

/* line 755 "l_scan.c" */
} goto yyBegin;
case 33:;
yySetPosition
/* line 240 "l.rex" */
{
    yyStart (STRING);
    stringLength = 0;

/* line 764 "l_scan.c" */
} goto yyBegin;
case 10:;
case 24:;
case 28:;
yySetPosition
/* line 246 "l.rex" */
{
    stringLength += l_scan_GetWord (&string[stringLength]);

/* line 774 "l_scan.c" */
} goto yyBegin;
case 32:;
yySetPosition
/* line 251 "l.rex" */
{
    yyStart(STD);
    string[stringLength] = '\0';
    l_scan_Attribute.string_const.Value = malloc (stringLength);
    strcpy (l_scan_Attribute.string_const.Value, string);
    return tok_string_const;

/* line 786 "l_scan.c" */
} goto yyBegin;
case 31:;
yySetPosition
/* line 260 "l.rex" */
{
    string[stringLength++] = '"';

/* line 794 "l_scan.c" */
} goto yyBegin;
case 26:;
{/* BlankAction */
while (* yyChBufferIndexReg ++ == ' ') ;
l_scan_TokenPtr = (yytChar *) -- yyChBufferIndexReg;
yyState = yyStartState;
yyStatePtr = & yyStateStack [1];
goto yyContinue;
/* line 803 "l_scan.c" */
} goto yyBegin;
case 23:;
{/* TabAction */
yyTab;
/* line 808 "l_scan.c" */
} goto yyBegin;
case 22:;
{/* EolAction */
yyEol (0);
/* line 813 "l_scan.c" */
} goto yyBegin;
case 1:;
case 2:;
case 3:;
case 4:;
case 5:;
case 6:;
case 7:;
case 8:;
case 16:;
case 18:;
case 19:;
case 30:;
case 41:;
case 44:;
case 45:;
	 /* non final states */
	 yyChBufferIndexReg --;			/* return character */
	 break;

case 21:
	 yySetPosition
      l_scan_TokenLength   = 1;
	 yyChBufferIndex = ++ yyChBufferIndexReg;
	 {
/* line 90 "l.rex" */

  /* What happens if no scanner rule matches the input */
  WritePosition (stderr, l_scan_Attribute.Position);
  fprintf (stderr, " Illegal character [%c]\n", *l_scan_TokenPtr);

/* line 845 "l_scan.c" */
	 }
	 goto yyBegin;

      case yyDNoState:
	 goto yyBegin;

case 20:
	 yyChBufferIndex = -- yyChBufferIndexReg; /* undo last state transit */
	 if (-- l_scan_TokenLength == 0) {		/* get previous state */
	    yyState = yyStartState;
	 } else {
	    yyState = * (yyStatePtr - 1);
	 }

	 if (yyChBufferIndex != & yyChBufferStart [yyBytesRead]) {
					/* end of buffer sentinel in buffer */
	    if ((yyState = yyEobTrans [yyState]) == yyDNoState) continue;
	    yyChBufferIndexReg ++;
	    * yyStatePtr ++ = yyState;		/* push state */
	    goto yyContinue;
	 }
						/* end of buffer reached */
	 if (! yyEof) {
	    register yytChar * yySource;
	    register yytChar * yyTarget;
	    unsigned long yyChBufferFree;

	    if (yyChBufferSize == 0) {
	       yyStateRange * yyOldStateStack = yyStateStack;
	       yyInitialize ();
	       yyStatePtr += yyStateStack - yyOldStateStack;
	       yyChBufferIndexReg = yyChBufferIndex;
	    }
	    yySource = l_scan_TokenPtr - 1;
	    yyTarget = (yytChar *) & yyChBufferPtr
		[(yyMaxAlign - 1 - l_scan_TokenLength) & (yyMaxAlign - 1)];
	    yyChBufferFree = Exp2 (Log2 (yyChBufferSize - 4 -
		yyMaxAlign - l_scan_TokenLength));
		/* copy initial part of token in front of the input buffer */
	    if (yySource > yyTarget) {
	       l_scan_TokenPtr = yyTarget + 1;
	       do * yyTarget ++ = * yySource ++;
	       while (yySource < (yytChar *) yyChBufferIndexReg);
	       yyLineStart += (yytusChar *) yyTarget - yyChBufferStart -
				yyBytesRead;
	       yyChBufferStart = (yytusChar *) yyTarget;
	    } else {
	       yyChBufferStart = yyChBufferIndexReg;
	    }
	    yyChBufferStart2 = (yytChar *) yyChBufferStart;
						/* extend buffer if necessary */
	    if (yyChBufferFree < yyChBufferSize >> 3 /* / 8 */ ) {
	       register ptrdiff_t yyDelta;
	       register yytusChar * yyOldChBufferPtr = yyChBufferPtr;
	       ExtendArray ((char * *) & yyChBufferPtr, & yyChBufferSize,
				(unsigned long) sizeof (yytChar));
	       if (yyChBufferPtr == NULL) yyErrorMessage (xxScannerOutOfMemory);
	       yyDelta = yyChBufferPtr - yyOldChBufferPtr;
	       yyChBufferStart	+= yyDelta;
	       yyLineStart	+= yyDelta;
	       l_scan_TokenPtr	+= yyDelta;
	       yyChBufferStart2	 = (yytChar *) yyChBufferStart;
	       yyChBufferFree = Exp2 (Log2 (yyChBufferSize - 4 -
			yyMaxAlign - l_scan_TokenLength));
	       if (yyStateStackSize < yyChBufferSize) {
		  yyStateRange * yyOldStateStack = yyStateStack;
		  ExtendArray ((char * *) & yyStateStack, & yyStateStackSize,
				   (unsigned long) sizeof (yyStateRange));
		  if (yyStateStack == NULL)
		     yyErrorMessage (xxScannerOutOfMemory);
		  yyStatePtr	+= yyStateStack - yyOldStateStack;
	       }
	    }
						/* read buffer and restart */
	    yyChBufferIndex = yyChBufferIndexReg = yyChBufferStart;
	    yyFileOffset += yyBytesRead;
	    yyBytesRead = yyGetLine (yySourceFile, (yytChar *) yyChBufferIndex,
	       (int) yyChBufferFree);
	    if (yyBytesRead <= 0) { yyBytesRead = 0; yyEof = rtrue; }
	    yyChBufferStart [yyBytesRead    ] = yyEobCh;
	    yyChBufferStart [yyBytesRead + 1] = '\0';
	    goto yyContinue;
	 }

	 if (l_scan_TokenLength == 0) {		/* end of file reached */
	    if (yyChBufferSize == 0) return l_scan_EofToken;
	    yySetPosition
	    l_scan_CloseFile ();
# if yyInitFileStackSize != 0
	    if (yyFileStackPtr == yyFileStack) {
/* line 96 "l.rex" */

  /* What should be done if the end-of-input-file has been reached? */

  /* E.g.: check hat strings and comments are closed. */
  switch (yyStartState) {
  case STD:
    /* ok */
    break;
  case APP_NAME:
    WritePosition (stderr, l_scan_Attribute.Position);
    fprintf (stderr, " Nicht abgeschlossener App-Name\n");
   break;
  case STRING:
    WritePosition (stderr, l_scan_Attribute.Position);
    fprintf (stderr, " Nicht abgeschlossener String\n");
   break;
  case COMMENT:
    WritePosition (stderr, l_scan_Attribute.Position);
    fprintf (stderr, " Nicht abgeschlossener Kommentar\n");
    break;
  default:
    Message ("OOPS: that should not happen!!",
         xxFatal, l_scan_Attribute.Position);
    break;
  }

  /* implicit: return the EofToken */

/* line 965 "l_scan.c" */
	    }
	    if (yyFileStackPtr == yyFileStack) {
	       l_scan_ResetScanner ();
	       return l_scan_EofToken;
	    }
	    goto yyBegin;
# else
/* line 96 "l.rex" */

  /* What should be done if the end-of-input-file has been reached? */

  /* E.g.: check hat strings and comments are closed. */
  switch (yyStartState) {
  case STD:
    /* ok */
    break;
  case APP_NAME:
    WritePosition (stderr, l_scan_Attribute.Position);
    fprintf (stderr, " Nicht abgeschlossener App-Name\n");
   break;
  case STRING:
    WritePosition (stderr, l_scan_Attribute.Position);
    fprintf (stderr, " Nicht abgeschlossener String\n");
   break;
  case COMMENT:
    WritePosition (stderr, l_scan_Attribute.Position);
    fprintf (stderr, " Nicht abgeschlossener Kommentar\n");
    break;
  default:
    Message ("OOPS: that should not happen!!",
         xxFatal, l_scan_Attribute.Position);
    break;
  }

  /* implicit: return the EofToken */

/* line 1002 "l_scan.c" */
	    l_scan_ResetScanner ();
	    return l_scan_EofToken;
# endif
	 }
	 break;

      default:
	 yyErrorMessage (xxScannerInternalError);
      }
   }
}

static void yyInitialize ARGS ((void))
   {
      if (yyStateStackSize == 0) {
	 yyStateStackSize = yyInitBufferSize;
	 MakeArray ((char * *) & yyStateStack, & yyStateStackSize,
		       (unsigned long) sizeof (yyStateRange));
	 if (yyStateStack == NULL) yyErrorMessage (xxScannerOutOfMemory);
	 yyStateStack [0] = yyDefaultState;
      }
# if yyInitFileStackSize != 0
      if (yyFileStackSize == 0) {
	 yyFileStackSize = yyInitFileStackSize;
	 MakeArray ((char * *) & yyFileStack, & yyFileStackSize,
			(unsigned long) sizeof (yytFileStack));
	 if (yyFileStack == NULL) yyErrorMessage (xxScannerOutOfMemory);
	 yyFileStackPtr = yyFileStack;
      }

      if (yyFileStackPtr >= yyFileStack + yyFileStackSize - 1) {
	 ptrdiff_t yyyFileStackPtr = yyFileStackPtr - yyFileStack;
	 ExtendArray ((char * *) & yyFileStack, & yyFileStackSize,
			   (unsigned long) sizeof (yytFileStack));
	 if (yyFileStack == NULL) yyErrorMessage (xxScannerOutOfMemory);
	 yyFileStackPtr = yyFileStack + yyyFileStackPtr;
      }
      yyFileStackPtr ++;			/* push file */
      yyFileStackPtr->yySourceFile	= yySourceFile		;
      yyFileStackPtr->yyEof		= yyEof			;
      yyFileStackPtr->yyChBufferPtr	= yyChBufferPtr		;
      yyFileStackPtr->yyChBufferStart	= yyChBufferStart	;
      yyFileStackPtr->yyChBufferSize	= yyChBufferSize	;
      yyFileStackPtr->yyChBufferIndex	= yyChBufferIndex	;
      yyFileStackPtr->yyBytesRead	= yyBytesRead		;
      yyFileStackPtr->yyFileOffset	= yyFileOffset		;
      yyFileStackPtr->yyLineCount	= yyLineCount		;
      yyFileStackPtr->yyLineStart	= yyLineStart		;
# endif
						/* initialize file state */
      yyChBufferSize	   = yyInitBufferSize;
      MakeArray ((char * *) & yyChBufferPtr, & yyChBufferSize,
			(unsigned long) sizeof (yytChar));
      if (yyChBufferPtr == NULL) yyErrorMessage (xxScannerOutOfMemory);
      yyChBufferStart	   = & yyChBufferPtr [yyMaxAlign];
      yyChBufferStart2	   = (yytChar *) yyChBufferStart;
      yyChBufferStart [-1] = yyEolCh;		/* begin of line indicator */
      yyChBufferStart [ 0] = yyEobCh;		/* end of buffer sentinel */
      yyChBufferStart [ 1] = '\0';
      yyChBufferIndex	   = yyChBufferStart;
      l_scan_TokenPtr	   = (yytChar *) yyChBufferStart;
      yyEof		   = rfalse;
      yyBytesRead	   = 0;
      yyFileOffset	   = 0;
      yyLineCount	   = 1;
      yyLineStart	   = & yyChBufferStart [-1];
# ifdef HAVE_FILE_NAME
      if (l_scan_Attribute.Position.FileName == 0)
	 l_scan_Attribute.Position.FileName = 1; /* NoIdent */
# endif
   }

void l_scan_BeginFile
# ifdef HAVE_ARGS
   (char * yyFileName)
# else
   (yyFileName) char * yyFileName;
# endif
   {
      yyInitialize ();
      yySourceFile = yyFileName == NULL ? StdIn :
# ifdef SOURCE_VER
	 l_scan_BeginSourceFile (yyFileName);
# else
	 l_scan_BeginSource (yyFileName);
# endif
      if (yySourceFile < 0) yyErrorMessage (xxCannotOpenInputFile);
   }

# ifdef SOURCE_VER

# if HAVE_WCHAR_T

void l_scan_BeginFileW
# ifdef HAVE_ARGS
   (wchar_t * yyFileName)
# else
   (yyFileName) wchar_t * yyFileName;
# endif
   {
      yyInitialize ();
      yySourceFile = yyFileName == NULL ? StdIn :
	 l_scan_BeginSourceFileW (yyFileName);
      if (yySourceFile < 0) yyErrorMessage (xxCannotOpenInputFile);
   }

# endif

void l_scan_BeginMemory
# ifdef HAVE_ARGS
   (void * yyInputPtr)
# else
   (yyInputPtr) void * yyInputPtr;
# endif
   {
      yyInitialize ();
      l_scan_BeginSourceMemory (yyInputPtr);
   }

void l_scan_BeginMemoryN
# ifdef HAVE_ARGS
   (void * yyInputPtr, int yyLength)
# else
   (yyInputPtr, yyLength) void * yyInputPtr; int yyLength;
# endif
   {
      yyInitialize ();
      l_scan_BeginSourceMemoryN (yyInputPtr, yyLength);
   }

void l_scan_BeginGeneric
# ifdef HAVE_ARGS
   (void * yyInputPtr)
# else
   (yyInputPtr) void * yyInputPtr;
# endif
   {
      yyInitialize ();
      l_scan_BeginSourceGeneric (yyInputPtr);
   }

# endif

void l_scan_CloseFile ARGS ((void))
   {
# if yyInitFileStackSize != 0
      if (yyFileStackPtr == yyFileStack) yyErrorMessage (xxFileStackUnderflow);
# endif
      l_scan_CloseSource (yySourceFile);
      ReleaseArray ((char * *) & yyChBufferPtr, & yyChBufferSize,
			(unsigned long) sizeof (yytChar));
# if yyInitFileStackSize != 0
						/* pop file */
      yySourceFile	= yyFileStackPtr->yySourceFile		;
      yyEof		= yyFileStackPtr->yyEof			;
      yyChBufferPtr	= yyFileStackPtr->yyChBufferPtr		;
      yyChBufferStart	= yyFileStackPtr->yyChBufferStart	;
      yyChBufferStart2	= (yytChar *) yyChBufferStart		;
      yyChBufferSize	= yyFileStackPtr->yyChBufferSize	;
      yyChBufferIndex	= yyFileStackPtr->yyChBufferIndex	;
      yyBytesRead	= yyFileStackPtr->yyBytesRead		;
      yyFileOffset	= yyFileStackPtr->yyFileOffset		;
      yyLineCount	= yyFileStackPtr->yyLineCount		;
      yyLineStart	= yyFileStackPtr->yyLineStart		;
      yyFileStackPtr --;
# endif
   }

# ifdef xxGetWord
int l_scan_GetWord
# ifdef HAVE_ARGS
   (yytChar * yyWord)
# else
   (yyWord) yytChar * yyWord;
# endif
   {
      register yytChar * yySource		= l_scan_TokenPtr;
      register yytChar * yyTarget		= yyWord;
      register yytChar * yyChBufferIndexReg	= (yytChar *) yyChBufferIndex;

      while (yySource < yyChBufferIndexReg)
	 * yyTarget ++ = * yySource ++;
      * yyTarget = '\0';
      return (int) (yyChBufferIndexReg - l_scan_TokenPtr);
   }
# endif

# ifdef xxGetLower
int l_scan_GetLower
# ifdef HAVE_ARGS
   (yytChar * yyWord)
# else
   (yyWord) yytChar * yyWord;
# endif
   {
      register yytusChar * yySource	= (yytusChar *) l_scan_TokenPtr;
      register yytusChar * yyTarget	= (yytusChar *) yyWord;
      register yytusChar * yyChBufferIndexReg = yyChBufferIndex;

      while (yySource < yyChBufferIndexReg)
	 * yyTarget ++ = yyToLower (* yySource ++);
      * yyTarget = '\0';
      return (int) (yyChBufferIndexReg - (yytusChar *) l_scan_TokenPtr);
   }
# endif

# ifdef xxGetUpper
int l_scan_GetUpper
# ifdef HAVE_ARGS
   (yytChar * yyWord)
# else
   (yyWord) yytChar * yyWord;
# endif
   {
      register yytusChar * yySource	= (yytusChar *) l_scan_TokenPtr;
      register yytusChar * yyTarget	= (yytusChar *) yyWord;
      register yytusChar * yyChBufferIndexReg = yyChBufferIndex;

      while (yySource < yyChBufferIndexReg)
	 * yyTarget ++ = yyToUpper (* yySource ++);
      * yyTarget = '\0';
      return (int) (yyChBufferIndexReg - (yytusChar *) l_scan_TokenPtr);
   }
# endif

# ifdef xxinput
static yytChar input ARGS ((void))
   {
      if (yyChBufferIndex == & yyChBufferStart [yyBytesRead]) {
	 if (! yyEof) {
	    yyLineStart -= yyBytesRead;
	    yyChBufferIndex = yyChBufferStart = & yyChBufferPtr [yyMaxAlign];
	    yyChBufferStart2 = (yytChar *) yyChBufferStart;
	    yyFileOffset += yyBytesRead;
	    yyBytesRead = yyGetLine (yySourceFile, (yytChar *) yyChBufferIndex,
	       (int) Exp2 (Log2 (yyChBufferSize)));
	    if (yyBytesRead <= 0) { yyBytesRead = 0; yyEof = rtrue; }
	    yyChBufferStart [yyBytesRead    ] = yyEobCh;
	    yyChBufferStart [yyBytesRead + 1] = '\0';
	 }
      }
      if (yyChBufferIndex == & yyChBufferStart [yyBytesRead]) return '\0';
      else return * yyChBufferIndex ++;
   }
# endif

static void unput
# ifdef HAVE_ARGS
   (yytChar yyc)
# else
   (yyc) yytChar yyc;
# endif
   { * (-- yyChBufferIndex) = yyc; }

static void yyLess
# ifdef HAVE_ARGS
   (int yyn)
# else
   (yyn) int yyn;
# endif
   { yyChBufferIndex -= l_scan_TokenLength - yyn; l_scan_TokenLength = yyn; }

void l_scan_BeginScanner ARGS ((void))
   {
   }

void l_scan_CloseScanner ARGS ((void))
   {
   }

void l_scan_ResetScanner ARGS ((void))
   {
      yyChBufferPtr	= yyInitChBuffer;
      yyChBufferSize	= 0;
      yyChBufferStart	= & yyInitChBuffer [16];
      yyChBufferIndex	= & yyInitChBuffer [16];
      if (yyStateStackSize != 0)
	 ReleaseArray ((char * *) & yyStateStack, & yyStateStackSize,
			(unsigned long) sizeof (yyStateRange));
      yyStateStack	= yyInitStateStack;
      yyStateStackSize	= 0;
# if yyInitFileStackSize != 0
      if (yyFileStackSize != 0)
	 ReleaseArray ((char * *) & yyFileStack, & yyFileStackSize,
			(unsigned long) sizeof (yytFileStack));
      yyFileStackSize	= 0;
# endif
# if defined xxyyPush | defined xxyyPop
      if (yyStStStackSize != 0)
	 ReleaseArray ((char * *) & yyStStStackPtr, & yyStStStackSize,
			(unsigned long) sizeof (yyStateRange));
      yyStStStackSize	= 0;
      yyStStStackIdx	= 0;
# endif
      yyStartState	= STD;
      yyPreviousStart	= STD;
      yySourceFile	= StdIn;
   }

static void yyErrorMessage
# ifdef HAVE_ARGS
   (int yyErrorCode)
# else
   (yyErrorCode) int yyErrorCode;
# endif
   {
      ErrorMessageI (yyErrorCode, xxFatal, l_scan_Attribute.Position,
	 xxString, "l_scan");
      l_scan_Exit ();
   }
