/**
 * Samuel Philipp
 * Micha Piertzik
 * Jan-Eric Gaidusch
 */

/* Project:  COCKTAIL training
 * Descr:    LR parser for an expression language
 * Kind:     Parser specification (stub)
 * Author:   Dr. Juergen Vollmer <Juergen.Vollmer@informatik-vollmer.de>
 * $Id: expr.pars,v 1.4 2007/06/01 12:04:31 vollmer Exp vollmer $
 */

SCANNER expr_scan

PARSER  expr_pars

GLOBAL {
 # include <stdio.h>
}

PREC
NONE '<' '>' '=='
LEFT '-' '+'
LEFT '*' '/'

RULE

root = x.

x = < 
    = name declpart exepart
    .
> .

name = < 
    = '#' identifier ';'
    .
> .

declpart = <
    = decl declpart
    .
    =
    .
> .

decl = <
    = vartyp decloption identifier
    .
> .

vartyp = <
    = key
    .
> .

decloption = <
    = 'in'
    .
    = 'out'
    .
    = 'both'
    .
    =
    .
> .

exepart = <
    = statement exepart
    .
    =
    .
> .

statement = <
    = identifier '=' expr
    .
    = 'begin' statement_l 'end'
    .
    = ifstatement
    .
    = forstatement
    .
    = whilestatement
    .
> .

expr = <
    = l:expr '+' r:expr
    .
    = l:expr '-' r:expr
    .
    = l:expr '*' r:expr
    .
    = l:expr '/' r:expr
    .
    = l:expr '<' r:expr
    .
    = l:expr '>' r:expr
    .
    = l:expr '==' r:expr
    .
    = '(' expr ')'
    .
    = int_const
    .
    = float_const
    .
    = string_const
    .
    = identifier
    .
> .

statement_l = <
    = statement statement_l
    .
    = statement
    .
> .

forstatement = <
    = 'for' '(' int_const ')' statement
    .
> .

ifstatement = <
    = 'if' '(' expr ')' statement
    .
    = 'if' '(' expr ')' statement 'else' statement
    .
> .

whilestatement = <
    = 'while' '(' expr ')' statement
    .
> .

/* Tokens */
key:   [Value: tIdent] {Value := NoIdent;} .
int_const:    [Value: long]   {Value := 0;  } .
float_const:  [Value: double] {Value := 0.0;} .
string_const:   [Value: tIdent] {Value := NoIdent;} .
identifier:   [Value: tIdent] {Value := NoIdent;} .
